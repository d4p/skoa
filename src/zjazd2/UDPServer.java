package zjazd2;

import java.net.*;
import java.util.Arrays;

class UDPServer {
	public final static int lPort = 9000;

	public static void main(String args[]) throws Exception {
		byte[] receiveData = new byte[1024];
		DatagramPacket receivePacket = new DatagramPacket(receiveData,
				receiveData.length);
		System.out.println("LISTENING ON PORT " + lPort + "...");
		while (true) {
			DatagramSocket serverSocket = new DatagramSocket(lPort);
			serverSocket.receive(receivePacket);
			InetAddress IPAddress = receivePacket.getAddress();
			int rPort = receivePacket.getPort();
			String message = new String(receivePacket.getData());
			System.out.println("RECEIVED UDP MESSAGE FROM: " + IPAddress + ":"
					+ rPort + "\nCONTENT:\n" + message);
			serverSocket.close();
			Arrays.fill(receiveData, (byte) 0);
		}
	}
}